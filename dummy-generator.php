<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 12/08/2017
 * Time: 10:54
 */
require 'vendor/autoload.php';
include __DIR__ . '/config/config.php';
include __DIR__ . '/console/util/spacing.php';

use Console\Src\DummyGenerator;

$generator = new DummyGenerator();

$generator->getInitAllTableModels();