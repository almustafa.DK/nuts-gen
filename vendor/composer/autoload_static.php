<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2166b8813295cd839f756695fdc26134
{
    public static $files = array (
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
        '5255c38a0faeba867671b61dfda6d864' => __DIR__ . '/..' . '/paragonie/random_compat/lib/random.php',
        '72579e7bd17821bb1321b87411366eae' => __DIR__ . '/..' . '/illuminate/support/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\Translation\\' => 30,
            'Symfony\\Component\\Debug\\' => 24,
            'Symfony\\Component\\Console\\' => 26,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
        ),
        'I' => 
        array (
            'Illuminate\\Support\\' => 19,
            'Illuminate\\Database\\' => 20,
            'Illuminate\\Contracts\\' => 21,
            'Illuminate\\Container\\' => 21,
        ),
        'F' => 
        array (
            'Faker\\' => 6,
        ),
        'C' => 
        array (
            'Carbon\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\Translation\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/translation',
        ),
        'Symfony\\Component\\Debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/debug',
        ),
        'Symfony\\Component\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/console',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'Illuminate\\Support\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/support',
        ),
        'Illuminate\\Database\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/database',
        ),
        'Illuminate\\Contracts\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/contracts',
        ),
        'Illuminate\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/illuminate/container',
        ),
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
        'Carbon\\' => 
        array (
            0 => __DIR__ . '/..' . '/nesbot/carbon/src/Carbon',
        ),
    );

    public static $prefixesPsr0 = array (
        'D' => 
        array (
            'Doctrine\\Common\\Inflector\\' => 
            array (
                0 => __DIR__ . '/..' . '/doctrine/inflector/lib',
            ),
        ),
    );

    public static $classMap = array (
        'Base\\BaseController' => __DIR__ . '/../..' . '/console/src/library/base/BaseController.php',
        'Base\\BaseHelper' => __DIR__ . '/../..' . '/console/src/library/base/BaseHelper.php',
        'Base\\BaseModel' => __DIR__ . '/../..' . '/console/src/library/base/BaseModel.php',
        'Console\\Command\\ControllerGeneratorCommand' => __DIR__ . '/../..' . '/console/command/ControllerGeneratorCommand.php',
        'Console\\Command\\ModelGeneratorCommand' => __DIR__ . '/../..' . '/console/command/ModelGeneratorCommand.php',
        'Console\\Command\\ProjectBuilderCommand' => __DIR__ . '/../..' . '/console/command/ProjectBuilderCommand.php',
        'Console\\Command\\ServiceGeneratorCommand' => __DIR__ . '/../..' . '/console/command/ServiceGeneratorCommand.php',
        'Console\\ProjectBuilder' => __DIR__ . '/../..' . '/console/ProjectBuilder.php',
        'Console\\Src\\ClassGenerator' => __DIR__ . '/../..' . '/console/src/ClassGenerator.php',
        'Console\\Src\\Connection\\Connection' => __DIR__ . '/../..' . '/console/src/connection/Connection.php',
        'Console\\Src\\ControllerGenerator' => __DIR__ . '/../..' . '/console/src/ControllerGenerator.php',
        'Console\\Src\\DummyGenerator' => __DIR__ . '/../..' . '/console/src/DummyGenerator.php',
        'Console\\Src\\GeneralSystemGenerator' => __DIR__ . '/../..' . '/console/src/GeneralSystemGenerator.php',
        'Console\\Src\\GeneralUserGenerator' => __DIR__ . '/../..' . '/console/src/GeneralUserGenerator.php',
        'Console\\Src\\HelperGenerator' => __DIR__ . '/../..' . '/console/src/HelperGenerator.php',
        'Console\\Src\\Helper\\MethodModifer' => __DIR__ . '/../..' . '/console/src/helper/MethodModifer.php',
        'Console\\Src\\Helper\\MethodValidationModifier' => __DIR__ . '/../..' . '/console/src/helper/MethodValidationModifier.php',
        'Console\\Src\\Helper\\ModelMethodModifier' => __DIR__ . '/../..' . '/console/src/helper/ModelMethodModifier.php',
        'Console\\Src\\Helper\\PropertyProfiler' => __DIR__ . '/../..' . '/console/src/helper/PropertyProfiler.php',
        'Console\\Src\\Helper\\UnitTestMethodModifier' => __DIR__ . '/../..' . '/console/src/helper/UnitTestMethodModifier.php',
        'Console\\Src\\Helper\\UnitTestModifier' => __DIR__ . '/../..' . '/console/src/helper/UnitTestModifier.php',
        'Console\\Src\\ModelGenerator' => __DIR__ . '/../..' . '/console/src/ModelGenerator.php',
        'Console\\Src\\RoutingGenerator' => __DIR__ . '/../..' . '/console/src/RoutingGenerator.php',
        'Console\\Src\\SlimAutoConfigGenerator' => __DIR__ . '/../..' . '/console/src/SlimAutoConfigGenerator.php',
        'Console\\Src\\UnitTestingGenerator' => __DIR__ . '/../..' . '/console/src/UnitTestingGenerator.php',
        'Console\\util\\GeneralHandler' => __DIR__ . '/../..' . '/console/util/GeneralHandler.php',
        'Console\\util\\StubGenerator' => __DIR__ . '/../..' . '/console/util/StubGenerator.php',
        'Handler\\DataValidationException' => __DIR__ . '/../..' . '/console/src/library/handler/DataValidationException.php',
        'Provider\\Auth\\SessionUserTrait' => __DIR__ . '/../..' . '/console/src/library/provider/auth/SessionUserTrait.php',
        'Provider\\General\\GeneralHandler' => __DIR__ . '/../..' . '/console/src/library/provider/general/GeneralHandler.php',
        'Provider\\IAM\\Core\\Auth\\Base\\BaseHelperAuth' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/base/BaseHelperAuth.php',
        'Provider\\IAM\\Core\\Auth\\General\\GeneralUserAuthHelper' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/general/GeneralUserAuthHelper.php',
        'Provider\\IAM\\Core\\Auth\\General\\GeneralUserAuthModel' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/general/GeneralUserAuthModel.php',
        'Provider\\IAM\\Core\\Auth\\General\\GeneralUserAuthTrait' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/general/GeneralUserAuthTrait.php',
        'Provider\\IAM\\Core\\Auth\\System\\SystemUserAuthHelper' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/system/SystemUserAuthHelper.php',
        'Provider\\IAM\\Core\\Auth\\System\\SystemUserAuthModel' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/system/SystemUserAuthModel.php',
        'Provider\\IAM\\Core\\Auth\\System\\SystemUserAuthTrait' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/auth/system/SystemUserAuthTrait.php',
        'Provider\\IAM\\Core\\Model\\CoreModel' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/model/CoreModel.php',
        'Provider\\IAM\\Core\\Model\\Pagination' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/model/Pagination.php',
        'Provider\\IAM\\Core\\Request\\CoreRequest' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/request/CoreRequest.php',
        'Provider\\IAM\\Core\\Response\\CoreResponse' => __DIR__ . '/../..' . '/console/src/library/provider/iam/core/response/CoreResponse.php',
        'Provider\\Pagination\\Pagination' => __DIR__ . '/../..' . '/console/src/library/provider/pagination/Pagination.php',
        'Provider\\Response\\Results' => __DIR__ . '/../..' . '/console/src/library/provider/response/Results.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2166b8813295cd839f756695fdc26134::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2166b8813295cd839f756695fdc26134::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit2166b8813295cd839f756695fdc26134::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit2166b8813295cd839f756695fdc26134::$classMap;

        }, null, ClassLoader::class);
    }
}
