<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 18:00
 */

namespace Console\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ControllerGeneratorCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:controller-generator')
            ->setDescription('Generating Controller for your default scheme')
            ->setHelp('This command allow you to create controller by default');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }


}