<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 18:15
 */

namespace Console\Command;


use Symfony\Component\Console\Command\Command;

class ServiceGeneratorCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:service-generating')
            ->setDescription('Generating service for your default scheme')
            ->setHelp('This command allow you to create service by default');
    }
}