<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 18:12
 */

namespace Console\Command;


use Symfony\Component\Console\Command\Command;

class ModelGeneratorCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:model-generator')
            ->setDescription('Generating model for your default scheme')
            ->setHelp('This command allow you to create model by default');;

    }
}