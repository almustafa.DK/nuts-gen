<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 18:12
 */

namespace Console\Command;


use Symfony\Component\Console\Command\Command;

class ProjectBuilderCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:project-builder')
            ->setDescription('Generating project for your default scheme')
            ->setHelp('This command allow you to create project by default');
    }
}