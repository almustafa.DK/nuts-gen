<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 11/08/2017
 * Time: 23:06
 */
define("single_line", "\n");
define("double_line", "\n\n");
define("triple_line", "\n\n\n");

define("single_tab", "\t");
define("double_tab", "\t\t");
define("triple_tab", "\t\t\t");