<?php
namespace Console\util;

/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 10:20
 */


use RuntimeException;

class StubGenerator
{
	/**
	 * @var string
	 */
	protected $source;
	/**
	 * @var string
	 */
	protected $target;

	/**
	 * @param string $source
	 * @param string $target
	 */
	public function __construct($source, $target)
	{
		$this->source = $source;
		$this->target = $target;
	}

	/**
	 * @param array $replacements
	 *
	 * @throws \RuntimeException
	 */
	public function render(array $replacements)
	{
		if (file_exists($this->target)) {
			echo 'Cannot generate file. Target ' . $this->target . ' already exists.';
		}
		$contents = file_get_contents($this->source);
		// Standard replacements
		collect($replacements)->each(function ( $replacement, $tag) use (&$contents) {
			$contents = str_replace($tag, $replacement, $contents);
		});
		$path = pathinfo($this->target, PATHINFO_DIRNAME);
		if (!file_exists($path)) {
			mkdir($path, 0776, true);
		}
		file_put_contents($this->target, $contents);
	}
}