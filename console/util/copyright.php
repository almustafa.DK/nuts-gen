<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 07/08/2017
 * Time: 22:25
 */
$config = [
	"createdBy" => "nuts-generator",
	"description" => "This project has been generate by nuts-generator",
	"author" => [
		"name" => "fahmi sulaiman",
		"email" => "sulaimanfahmi@gmail.com"
	],
	"type" => "project"
];
return $config;