<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 07/08/2017
 * Time: 21:51
 */

namespace Console;


use Console\Src\ControllerGenerator;
use Console\Src\HelperGenerator;
use Console\Src\ModelGenerator;
use Console\Src\RoutingGenerator;

class ProjectBuilder
{
	public $path = __DIR__ . "/../app/";
	private $copyRight;
	public $env = "environment";

	public function __construct()
	{
		$this->copyRight = require __DIR__ . '/util/copyright.php';
	}

	public function generateProject()
	{
		$directoryProject = $this->path . "/" . $GLOBALS["config"]["project-name"];
		if (!file_exists($directoryProject)) {
			mkdir($directoryProject, 0700, true);
		}
		chdir($directoryProject);
		echo "Are you NUTS !!??", PHP_EOL;
		echo "Slow Down pick up beer and chill", PHP_EOL;
		echo "NUTS generating compose file .... for you", PHP_EOL;
		//$this->generateComposer();
		echo "NUTS  generating Structue Project ... for you", PHP_EOL;
		$this->generateStructurProject($directoryProject);
		echo "NUTS on generating model ...", PHP_EOL;
		$this->generateModel();
		echo "NUTS on generating helper ...", PHP_EOL;
		$this->generateHelper();
		echo "NUTS on generating controller ...", PHP_EOL;
		$this->generateController();
		echo "NUTS on preparing routing ...", PHP_EOL;
		$this->generateRouteApi();
		echo "NUTS on installing library ...", PHP_EOL;
		//exec('composer install');
		echo "NUTS on dumping file for optimizing library ...", PHP_EOL;
		exec("composer dump-autoload -o");
		//exec("php -S localhost:1234 index.php");
		echo "Everything done ....", PHP_EOL;
		echo "Cheers and get some smoke ....";

	}

	private function generateComposer()
	{
		$composerData = [
			"name" => $GLOBALS["config"]["project-name"],
			"description" => $this->copyRight["description"],
			"author" => [
				"name" => $this->copyRight["author"]["name"],
				"email" => $this->copyRight["author"]["email"],
			],
			"type" => "project",
			"autoload" => [
				"classmap" => [
					"model",
					"controller",
					"handler",
					"provider",
					"base",
					"helper"
				]
			],
			"require" => [
				"slim/slim" => "*",
				"illuminate/database" => "*",
				"phpmailer/phpmailer" => "^5.2",
				"monolog/monolog" => ">=1.6.0",
                "GuzzleHttp",
                "fzaninotto/faker"=> "^1.6"
			],
            "require-dev"=>[
                "phpunit/phpunit"=> "*",
                "guzzlehttp/guzzle"=> "*"
            ]
		];
		$fp = fopen('composer.json', 'w');
		fwrite($fp, json_encode($composerData));
		fclose($fp);
	}

	private function generateStructurProject($directoryProject)
	{
		$this->createEnv($directoryProject);

		mkdir($directoryProject . "/model", 0700, true);
		mkdir($directoryProject . "/handler", 0700, true);
		mkdir($directoryProject . "/provider", 0700, true);
		mkdir($directoryProject . "/helper", 0700, true);
		mkdir($directoryProject . "/controller", 0700, true);
        mkdir($directoryProject . "/test", 0700, true);
		$this->generatingLibrarySupport();
	}

	/*
	 * Create environment
	 */
	private function createEnv($directoryProject)
	{
		mkdir($directoryProject . "/config", 0700, true);
		chdir($directoryProject . "/config");
		$env = [
			"development" => [
				"settings" => [
					"displayErrorDetails" => TRUE,
					"log.debug" => TRUE,
					"log.enabled" => TRUE,
				],
				"database" => [
					"driver" => $GLOBALS["config"]["db"]["driver"],
					"host" => $GLOBALS["config"]["db"]["host"],
					"database" => $GLOBALS["config"]["db"]["database"],
					"username" => $GLOBALS["config"]["db"]["username"],
					"password" => $GLOBALS["config"]["db"]["password"],
					"charset" => $GLOBALS["config"]["db"]["charset"],
					"collation" => $GLOBALS["config"]["db"]["collation"],
					"prefix" => $GLOBALS["config"]["db"]["prefix"]
				],
				"mail" => [
					"MAIL_HOST" => "smtp.gmail.com",
					"MAIL_USERNAME" => "demogimi@gmail.com",
					"MAIL_PASSWORD" => "gimi12345",
					"MAIL_PORT" => 465,
					"MAIL_SMTPSecure" => "ssl",
					"MAIL_SENDER" => "demogimi@gmail.com",
					"MAIL_SENDER_ALIAS" => "GimiApp"
				],
			],
			"production" => [
				"settings" => [
					"displayErrorDetails" => false,
					"log.debug" => false,
					"log.enabled" => false,
				],
				"database" => [
					"driver" => $GLOBALS["config"]["db"]["driver"],
					"host" => $GLOBALS["config"]["db"]["host"],
					"database" => $GLOBALS["config"]["db"]["database"],
					"username" => $GLOBALS["config"]["db"]["username"],
					"password" => $GLOBALS["config"]["db"]["password"],
					"charset" => $GLOBALS["config"]["db"]["charset"],
					"collation" => $GLOBALS["config"]["db"]["collation"],
					"prefix" => $GLOBALS["config"]["db"]["prefix"]
				],
				"mail" => [
					"MAIL_HOST" => "smtp.gmail.com",
					"MAIL_USERNAME" => "demogimi@gmail.com",
					"MAIL_PASSWORD" => "gimi12345",
					"MAIL_PORT" => 465,
					"MAIL_SMTPSecure" => "ssl",
					"MAIL_SENDER" => "demogimi@gmail.com",
					"MAIL_SENDER_ALIAS" => "GimiApp"
				],
			]
		];
		$fp = fopen('env.php', 'w');
		fwrite($fp, "<?php");
		fwrite($fp, PHP_EOL);
		fwrite($fp, '$environment=[');
		fwrite($fp, PHP_EOL);
		foreach ($env as $key => $value) {
			fwrite($fp, sprintf("\t'%s'=>[", $key));
			fwrite($fp, PHP_EOL);
			foreach ($value as $index => $row) {
				fwrite($fp, sprintf("\t\t'%s'=>[", $index));
				fwrite($fp, PHP_EOL);
				foreach ($row as $x => $y) {
					fwrite($fp, sprintf("\t\t\t'%s'=>'%s',", $x, $y));
					fwrite($fp, PHP_EOL);
				}
				fwrite($fp, "\t\t],\n");
				fwrite($fp, PHP_EOL);
			}
			fwrite($fp, "\t],\n");
		}
		fwrite($fp, PHP_EOL);
		fwrite($fp, "];");
		fclose($fp);
		$this->generateAutoConfig();
		chdir($directoryProject);
	}

	public function generatingLibrarySupport()
	{
		$directory = __DIR__ . "/src/library";
		$dst = __DIR__ . "/../app/" . $GLOBALS["config"]["project-name"];
		$this->cpy($directory, $dst);

	}

	public function cpy($source, $dest)
	{
		if (is_dir($source)) {
			$dir_handle = opendir($source);
			while ($file = readdir($dir_handle)) {
				if ($file != "." && $file != "..") {
					if (is_dir($source . "/" . $file)) {
						if (!is_dir($dest . "/" . $file)) {
							mkdir($dest . "/" . $file);
						}
						$this->cpy($source . "/" . $file, $dest . "/" . $file);
					} else {
						copy($source . "/" . $file, $dest . "/" . $file);
					}
				}
			}
			closedir($dir_handle);
		} else {
			copy($source, $dest);
		}
	}

	private function generateModel()
	{
		$initModel = new ModelGenerator();
		$initModel->getAllInitModel();
	}

	private function generateHelper()
	{
		$initHelper = new HelperGenerator();
		$initHelper->getAllInitHelper();
	}

	private function generateController()
	{
		$initController = new ControllerGenerator();
		$initController->getAllInitController();
	}

	private function generateAutoConfig()
	{
		return;
	}

	private function generateRouteApi()
	{
		$initRoute = new RoutingGenerator();
		$initRoute->getInitAllRouting();
		return;
	}


}