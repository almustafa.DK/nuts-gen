<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 14:00
 */

namespace Console\Src;


use Console\Src\Connection\Connection;
use Console\Src\Helper\UnitTestModifier;
use Console\util\GeneralHandler;
use Console\util\StubGenerator;

class UnitTestingGenerator
{
    public $con;
    public $targetDir;
    public $initializeTest;
    public $initializeRoute;
    public $nameSpace;
    public $relationDown;

    public $unitIndex;
    public $unitShow;
    public $unitStore;
    public $unitDelete;
    public $unitUpdate;
    public $unitPaging;
    public $testModifier;
    public $model;

    public function __construct()
    {
        $this->con = new Connection();
        $this->initializeTest = $GLOBALS["config"]["model"];
        $this->targetDir = __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/test/";
        $this->nameSpace = "Test";

    }

    public function getAllInitUnitTest()
    {
        foreach ($this->initializeTest as $model) {
          //  echo "NUTS creating test unit " . GeneralHandler::createClassNameFromTable($model["table"]) . "Test", PHP_EOL;
            $this->initializeRoute = 'http://localhost:1234/' . lcfirst(urldecode($GLOBALS["config"]["project-name"])) . '/' . $model["routingName"] . '';
            //$this->baseModel = $model["alias"] . 'Model';
            $this->model = $model;
            $this->buildClassUnitTest($model);

        };
    }

    private function buildClassUnitTest($model)
    {
        $this->generateUnitTestMethod();//generating unit test untuk semua class
        $targetClass = GeneralHandler::createClassNameFromTable($model["table"]);
        $target = $this->targetDir . "/" . $targetClass . "Test.php";
        $stub = new StubGenerator(
            __DIR__ . '/../stub/UnitTestStub.stub',
            $target
        );
        $stub->render([
            ':CLASS_NAME:' => $targetClass,
            ':ROUTE_URL:' => $this->initializeRoute,
            ':INDEX_UNIT_TEST:' => $this->unitIndex,
            ':PAGING_UNIT_TEST:' => $this->unitPaging,
            ':STORE_UNIT_TEST:' => $this->unitStore,
            ':SHOW_UNIT_TEST:' => $this->unitShow,
            ':UPDATE_UNIT_TEST:' => $this->unitUpdate,
            ':DELETE_UNIT_TEST:' => $this->unitDelete,
        ]);
        $this->unitDelete = "";
        $this->unitUpdate = "";
        $this->unitStore = "";
        $this->unitPaging = "";
        $this->unitIndex = "";
        $this->unitShow = "";
    }

    private function generateUnitTestMethod()
    {
        $unitTestModifier = new UnitTestModifier($this->initializeRoute, $this->model);
        $this->unitIndex = $unitTestModifier->generateIndexUnitTest();
        $this->unitPaging = $unitTestModifier->generatePaggingUnittest();
        $this->unitStore = $unitTestModifier->generateStoreUnitTest();
        $this->unitShow = $unitTestModifier->generateShowUnitTest();
        $this->unitDelete = $unitTestModifier->generateDeleteUnitTest();
        $this->unitUpdate = $unitTestModifier->generateUpdateUnitTest();
    }


}