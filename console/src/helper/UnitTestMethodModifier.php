<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 23:36
 */

namespace Console\Src\Helper;


class UnitTestMethodModifier
{

    /*
     * YOU CAN PUT RESPONSE ATTRIBUTE FROM $response variable
     */
    public function requestUnitTest($method, $uri, $data = null)
    {
        $string = "";
        $string .= single_tab . '$response = $this->client->request("' . $method . '", ' . $uri . ');' . single_line;
        return $string;
    }

    public function requestUnitTestWithData($method, $uri, $data = null)
    {
        $string = "";
        $string .= single_tab . '$response = $this->client->request("' . $method . '", ' . $uri . ',' . $data . ');' . single_line;
        return $string;
    }

    public function assertTrueTest($condition)
    {
        $string = "";
        $string .= '$this->assertTrue(' . $condition . ');';
        return $string;
    }

    public function assertEqualTest($expectation, $realita)
    {
        $string = "";
        $string .= '$this->assertEquals(' . $expectation . ',' . $realita . ');';
        return $string;
    }


}