<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 12/08/2017
 * Time: 11:07
 */

namespace Console\Src\Helper;


use Carbon\Carbon;

class PropertyProfiler
{

	public function filteringProperty($field)
	{
		$faker = \Faker\Factory::create();
		if ($this->isChar($field->DATA_TYPE)) {
			if (strtolower($field->COLUMN_NAME) === "email") {
				return $faker->email;
			} else if ((strtolower($field->COLUMN_NAME) == "fname") || (strtolower($field->COLUMN_NAME) == "firstname")) {
				return $faker->firstName;
			} else if ((strtolower($field->COLUMN_NAME) == "lname") || (strtolower($field->COLUMN_NAME) == "lastname")) {
				return $faker->lastName;
			} else {
				return $faker->text(15);
			}
		} else if ($this->isDate($field->DATA_TYPE)) {
			if ($field->DATA_TYPE == "date") {
				return Carbon::now()->toDateString();
			} else if ($field->DATA_TYPE == "datetime") {
				return Carbon::now()->toDateTimeString();
			}
		} else if ($this->isNumerik($field->DATA_TYPE)) {
			if ($field->LENGTH == 1) {
				return 1;
			} else {
				return $faker->randomNumber(5);
			}
		} else if ($this->isDecimal($field->DATA_TYPE)) {
			if ($field->LENGTH == 1) {
				return 1;
			} else {
				return $faker->randomNumber(5);
			}
		} else if ($this->isTinyInt($field->DATA_TYPE)) {
			if ($field->LENGTH == 1) {
				return 1;
			} else {
				return 1;
			}
		}
	}


	public function isChar($field)
	{
		if ($field == "varchar") {
			return true;
		}
	}

	public function isDate($field)
	{
		if ($field == "date") {
			return true;
		} else if ($field == "datetime") {
			return true;
		}
	}

	public function idDateTime($field)
	{
		if ($field == "datetime") {
			return true;
		}
	}

	public function isNumerik($field)
	{
		if ($field == "int") {
			return true;
		}
	}

	public function isTinyInt($field)
	{
		if ($field == "tinyint") {
			return true;
		}
	}

	public function isDecimal($field)
	{
		if ($field == "decimal") {
			return true;
		}
	}

	public function isLengthLessThanTen($field)
	{
		if ($field <= $field) {
			return true;
		}
	}

	public function isLengthMoreThanTenLesThan25($field)
	{

		if (($field > 10) && ($field <= 25)) {
			return true;
		}
	}

	public function isLengthMoreThan25($field)
	{
		if (($field > 25)) {
			return true;
		}
	}


}