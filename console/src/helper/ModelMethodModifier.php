<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 18:55
 */

namespace Console\Src\Helper;


use Console\util\GeneralHandler;

class ModelMethodModifier
{

	public $joinTable = "join";
	public $join;
	public $relation;
	public $model;

	public function __construct($model, $join)
	{
		$this->model = $model;
		$this->join = $join;
	}

	public function createJoinRelationShip()
	{
		if (($this->join->REFRENCE_TABLE != null) && ($this->join->REFERENCED_COLUMN != null) && ($this->join->RELATION != null)) {
			$methodName = ("joinRelation". GeneralHandler::createClassNameFromTable($this->join->REFRENCE_TABLE));
			$joinType = $this->joinType();
			$string = "\t\t" . 'public function ' . $methodName . '($alias = "' . $this->join->REFRENCE_TABLE . '"){' . "\n";
			$string .= "\t\t\t" . sprintf('$this->query->%s("%s as $alias","$alias.%s","=","%s.%s");',
					$joinType, $this->join->REFRENCE_TABLE, $this->join->REFERENCED_COLUMN, $this->model["aliasTable"], $this->join->COLUMN_NAME);
			$string .= "\n\t\t" . 'return $this;';
			$string .= "\n\t\t" . '}' . "\n";
			return $string;
		}

	}


	private function joinType()
	{
		if (empty($this->relation["condition"])) {
			return "join";
		} else {
			if ($this->relation["condition"] == "leftJoin") {
				return "leftJoin";
			} else {
				return "join";
			}
		}
	}
}