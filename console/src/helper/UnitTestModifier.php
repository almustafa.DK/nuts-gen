<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 23:28
 */

namespace Console\Src\Helper;
include __DIR__ . "/../vendor.php";

use Console\Src\Connection\Connection;
use Console\util\GeneralHandler;

class UnitTestModifier
{
    protected $uri;
    protected $model;
    protected $unitTestModifier;
    protected $conn;
    protected $bodyData = [];

    public function __construct($route, $model)
    {
        $this->uri = $route;
        $this->model = $model;
        $this->unitTestModifier = new UnitTestMethodModifier();
        $this->conn = new Connection();
    }


    public function generateIndexUnitTest()
    {
        $string = "";
        $string .= $this->unitTestModifier->requestUnitTest("GET", '"' . $this->uri . '"');
        $string .= single_line;
        $string .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
        return $string;
    }

    public function generatePaggingUnittest()
    {
        $string = "";
        $string .= '$response = $this->client->request("POST","' . $this->uri . '/paging"' .
            ',[
                "json" => [
                  "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                    ]
                ]
            ]
        );';
        $string .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
        return $string;
    }

    public function generateShowUnitTest()
    {
        $string = "";
        $table = '\Model\\' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model';
        if (!class_exists($table)) {
            echo "class no exist";
        } else {
            echo $table, PHP_EOL;
            $objModel = new $table();
            $relation = $objModel->getFirst();
            /*
             * @todo ini kondisi dimana data kosong
             * lu mesti check lagi
             */
            if (!$relation) {
                return $string;
            }
            $fk = $objModel->getKeyName();
            $propery = $relation->$fk;

            $uri = '"' . $this->uri . '/' . $propery . '"';
            $string .= $this->unitTestModifier->requestUnitTest("GET", $uri, null);
            $string .= single_line;
            $string .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
            return $string;
        }

    }

    public function generateStoreUnitTest()
    {
        $string = "";
        $dataBody = $this->getBodyPost();
        //  echo $dataBody;
        return $dataBody;
    }

    private function getBodyPost()
    {
        $request = "";
        $string = "";
        $string .= single_line;
        $data = [];
        if ($this->model["relationExecute"] == true) {
            if (!empty($this->model["relationDown"])) {
                $string .= '["json"=>[';
                $string .= $this->setUpPropertyDataUnitWithRelation($this->model);
                $string .= ']]';
            }
//            echo $this->model["table"],PHP_EOL;
//            echo "ini relation execute true",PHP_EOL;
        } else {
            $string .= $this->generatePropertyDataUnitTestNoRelation();
            echo $this->model["table"], PHP_EOL;
            echo "ini relation execute false", PHP_EOL;
        }
        $request .= $this->unitTestModifier->requestUnitTestWithData("POST", '"' . $this->uri . '"', $string);
        $request .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
        return $request;
    }


    private function setUpPropertyDataUnitWithRelation($parentModel)
    {
        $string = "";
        $string .= '"' . GeneralHandler::createClassNameFromTable($parentModel["table"]) . '"=>[';
        $property = $this->conn->getRelationShipDetailWithAttribute($parentModel["table"]);
        $profiler = new PropertyProfiler();
        foreach ($property as $item) {
            if (($item->PK_FK !== "PK") && ($item->PK_FK !== "FK")) {
                $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $profiler->filteringProperty($item) . '",';
            } else if ($item->PK_FK == "FK") {
                $table = '\Model\\' . GeneralHandler::createClassNameFromTable($item->REFERENCED_TABLE_NAME) . 'Model';
                if (!class_exists($table)) {
                    echo "class no exist";
                } else {
                    $objModel = new $table();
                    $relation = $objModel->getFirst();
                    $fk = $objModel->getKeyName();
                    $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $relation->$fk . '",';
                }
            }
        }
        $string .= ']' . single_line;
        if (count($parentModel["relationDown"]) > 0) {
            if ($parentModel["relationExecute"] == true)
                $string .= ',';
            foreach ($parentModel["relationDown"] as $model) {
                if ($parentModel["relationExecute"] == true)
                    $string .= single_line . $this->setUpRelationUnitData($model);
            }
        }
        return $string;

    }


    public function generateRelationUnitTest($parentModel, $model)
    {
        $string = "";
        $string .= '"' . GeneralHandler::createClassNameFromTable($model["table"]) . '"=>[';
        $property = $this->conn->getRelationShipDetailWithAttribute($model["table"]);
        $profiler = new PropertyProfiler();
        foreach ($property as $item) {
            if (($item->PK_FK !== "PK") && ($item->PK_FK !== "FK")) {
                $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $profiler->filteringProperty($item) . '",';
            } else if ($item->PK_FK == "FK") {
                $table = '\Model\\' . GeneralHandler::createClassNameFromTable($item->REFERENCED_TABLE_NAME) . 'Model';
                if (!class_exists($table)) {
                    echo "class no exist";
                } else {
                    $objModel = new $table();
                    $relation = $objModel->getFirst();
                    $fk = $objModel->getKeyName();
                    $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $relation->$fk . '",';
                }
            }
        }
        $string .= '';
        $string .= ']';
        if (!empty($model["relationDown"])) {
            if (count($model["relationDown"]) > 0) {
                $string .= ',';
                foreach ($model["relationDown"] as $key) {
                    $string .= $this->generateRelationUnitTest($model, $key);
                }
            }
        }

        return $string;
    }

    private function setUpRelationUnitData($model)
    {
        $string = "";
        if ($model["relationType"] == "oneToMany") {
            $string .= '"' . GeneralHandler::createClassNameFromTable($model["table"]) . '"=>[[';
            $property = $this->conn->getRelationShipDetailWithAttribute($model["table"]);
            $profiler = new PropertyProfiler();
            foreach ($property as $item) {
                $string .= single_line;
                if (($item->PK_FK !== "PK") && ($item->PK_FK !== "FK")) {
                    $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $profiler->filteringProperty($item) . '",';
                } else if ($item->PK_FK == "FK") {
                    $table = '\Model\\' . GeneralHandler::createClassNameFromTable($item->REFERENCED_TABLE_NAME) . 'Model';
                    if (!class_exists($table)) {
                        echo "class no exist";
                    } else {
                        $objModel = new $table();
                        $relation = $objModel->getFirst();
                        $fk = $objModel->getKeyName();
                        $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $relation->$fk . '",';
                    }
                }

            }
            $string .= ']]';
        } else {
            $string .= '["' . GeneralHandler::createClassNameFromTable($model["table"]) . '"=>[';
            $property = $this->conn->getRelationShipDetailWithAttribute($model["table"]);
            $profiler = new PropertyProfiler();
            foreach ($property as $item) {
                if (($item->PK_FK !== "PK") && ($item->PK_FK !== "FK")) {
                    $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $profiler->filteringProperty($item) . '",';
                } else if ($item->PK_FK == "FK") {
                    $table = '\Model\\' . GeneralHandler::createClassNameFromTable($item->REFERENCED_TABLE_NAME) . 'Model';
                    if (!class_exists($table)) {
                        echo "class no exist";
                    } else {
                        $objModel = new $table();
                        $relation = $objModel->getFirst();
                        $fk = $objModel->getKeyName();
                        $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $relation->$fk . '",';
                    }
                }
            }
            $string .= ']]';
        }
        if (!empty($model["relationDown"])) {
            if (count($model["relationDown"]) > 0) {
                $string .= ',';
                foreach ($model["relationDown"] as $key) {
                    $string .= $this->setUpRelationUnitData($key);
                }
            }
        }

        return $string;
    }

    private function generatePropertyDataUnitTestNoRelation()
    {
        $string = "";
        $string .= '["json"=>[' . '"' . GeneralHandler::createClassNameFromTable($this->model["table"]) . '"=>' . '[';
        $test = new Connection();
        $property = $test->getRelationShipDetailWithAttribute($this->model["table"]);
        $profiler = new PropertyProfiler();
        foreach ($property as $item) {

            if (($item->PK_FK !== "PK") && ($item->PK_FK !== "FK")) {
                $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $profiler->filteringProperty($item) . '",';
            } else if ($item->PK_FK == "FK") {
                $table = '\Model\\' . GeneralHandler::createClassNameFromTable($item->REFERENCED_TABLE_NAME) . 'Model';
                if (!class_exists($table)) {
                    echo "class no exist";
                } else {
                    $model = new $table();
                    $relation = $model->getFirst();
                    $fk = $model->getKeyName();
                    $string .= '"' . $item->COLUMN_NAME . '"' . '=>' . '"' . $relation->$fk . '",';
                    $data[GeneralHandler::createClassNameFromTable($this->model["table"])][$item->COLUMN_NAME] = $relation->$fk;
                }
            }
        }
        $string .= ']]]';
        return $string;
    }

    public function generateUpdateUnitTest()
    {
        $request = "";
        $string = "";
        $string .= single_line;
        $data = [];
        if ($this->model["relationExecute"] == true) {
            if (!empty($this->model["relationDown"])) {
                $string .= '["json"=>[';
                $string .= $this->setUpPropertyDataUnitWithRelation($this->model);
                $string .= ']]';
            }
        } else {
            $string .= $this->generatePropertyDataUnitTestNoRelation();
        }
        $table = '\Model\\' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model';
        if (!class_exists($table)) {
            echo "class no exist";
        } else {
            $objModel = new $table();
            $relation = $objModel->getFirst();
            $fk = $objModel->getKeyName();
            $propery = $relation->$fk;
            $request .= $this->unitTestModifier->requestUnitTestWithData("PUT", '"' . $this->uri . '/' . $propery . '/update"', $string);
            $request .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
            return $request;
        }
    }

    public function generateDeleteUnitTest()
    {
        $string = "";
        $table = '\Model\\' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model';
        /*
         * @todo check ketika classnya gak ada
         */
        if (!class_exists($table)) {
            echo "class no exist";
        } else {
            $objModel = new $table();
            $relation = $objModel->getFirst();
            /*
             * @todo ini handling ketika data dari relasi gak ada
             */
            if (!$relation) {
                return $string;
            }
            $fk = $objModel->getKeyName();
            $propery = $relation->$fk;

            $uri = '"' . $this->uri . '/' . $propery . '/delete"';
            $string .= '$response = $this->client->request("delete",' . $uri . ');';
            $string .= single_line;
            $string .= $this->unitTestModifier->assertEqualTest(200, '$response->getStatusCode()');
            return $string;
        }
    }


}