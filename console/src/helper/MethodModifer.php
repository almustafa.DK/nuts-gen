<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 13:18
 */

namespace Console\Src\Helper;


use Console\Src\Connection\Connection;
use Console\util\GeneralHandler;

class MethodModifer
{
    protected $model;
    protected $con;
    const framePublicFunction = 'public function ';
    const frameProtectedFunction = 'public static function';
    const framePrivateFunction = 'private function';

    public function __construct($model = null)
    {
        $this->model = $model;
        $this->con = new Connection();
    }


    public function store()
    {
        $string = "";
        $string .= self::framePublicFunction . 'store($input){' . single_line;
       // $string .= MethodValidationModifier::shortCutValidateBefore($this->model["table"]);
        $string .= double_tab . '$model = new ' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model($input["' . GeneralHandler::createClassNameFromTable($this->model["table"]) . '"]);' . single_line;
        $string .= double_tab . 'if(!$model->save()){' . single_line;
        $string .= double_tab . 'throw new \Exception();' . single_line;
        $string .= double_tab . '}' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    public function update()
    {
        $string = "";
        $string .= self::framePublicFunction . 'update($id,$input){' . single_line;
      //  $string .= MethodValidationModifier::shortCutValidateUpdate();
        $string .= double_tab . '$model =' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model::find($id);' . single_line;
        $string .= double_tab . '$model->setProperty($input["' . GeneralHandler::createClassNameFromTable($this->model["table"]) . '"]);' . single_line;
        $string .= double_tab . 'if(!$model->update()){' . single_line;
        $string .= double_tab . 'throw new \Exception();';
        $string .= double_tab . '}' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    public function storeWithRelation($parentModel)
    {
        $string = "";
        $string .= double_tab . self::framePublicFunction . 'store' . GeneralHandler::createClassNameFromTable($parentModel["table"]) . '($input){' . single_line;
        $string .= double_tab . '$' . lcfirst(GeneralHandler::createClassNameFromTable($parentModel["table"])) . ' = new ' . GeneralHandler::createClassNameFromTable($parentModel["table"]) . 'Model($input["' . GeneralHandler::createClassNameFromTable($this->model["table"]) . '"]);' . single_line;
        $string .= double_tab . 'if(!$' . lcfirst(GeneralHandler::createClassNameFromTable($parentModel["table"])) . '->save()){' . single_line;
        $string .= double_tab . 'throw new \Exception();' . single_line;
        $string .= double_tab . '}' . single_line;
        //echo $targetModel["table"], PHP_EOL;
        if (count($parentModel["relationDown"]) > 0) {
            if ($parentModel["relationExecute"] == true)
                foreach ($parentModel["relationDown"] as $model) {
                    if ($model["relationExecute"] == true)
                        $string .= triple_tab . $this->generateRelation($parentModel, $model);
                }
        }

        $string .= double_tab . 'return $' . lcfirst(GeneralHandler::createClassNameFromTable($parentModel["table"])) . ';' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    public function generateRelation($model, $relation)
    {
        $string = "";
        if ($relation["relationType"] == "oneToMany") {
            $string .= 'foreach($input["' . ucfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '"] as $key' . ucfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '){' . single_line;
            $string .= "\t\t\t\t" . '$' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) .
                '=' . 'new \Helper\\' . GeneralHandler::createClassNameFromTable($relation["table"]) . "Helper();";
            $string .= '$' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '= $' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '->' . 'storeFrom' .
                GeneralHandler::createClassNameFromTable($model["table"]) . '($' . lcfirst(GeneralHandler::createClassNameFromTable($model["table"])) . '->' . $relation["mapping"]["referenceKey"] .
                ',$key' . ucfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . ');' . single_line;
            $string .= '}' . single_line;

        } else {
            $string .= "\t\t\t\t" . '$' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) .
                '=' . 'new ' . GeneralHandler::createClassNameFromTable($relation["table"]) . "Helper();";
            $string .= '$' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '= $' . lcfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '->' . 'storeFrom' .
                GeneralHandler::createClassNameFromTable($model["table"]) . '($' . lcfirst(GeneralHandler::createClassNameFromTable($model["table"])) . '->' . $relation["mapping"]["referenceKey"] .
                ',$input["' . ucfirst(GeneralHandler::createClassNameFromTable($relation["table"])) . '"]);';
        }

        if (!empty($relation["relationDown"])) {
            if (count($relation["relationDown"]) > 0) {
                foreach ($relation["relationDown"] as $key) {
                    $string .= $this->generateRelation($relation, $key);
                }
            }
        }
        return $string;
    }

    public function storeFromRelation($targetModel, $model)
    {
        $string = "";
        $string .= self::framePublicFunction . 'storeFrom' . GeneralHandler::createClassNameFromTable($model["table"]) . '($' . $model["mapping"]["referenceKey"] . ',$input){' . single_line;
        $string .= double_tab . '$model = new ' . GeneralHandler::createClassNameFromTable($targetModel["table"]) . 'Model($input["' . GeneralHandler::createClassNameFromTable($this->model["table"]) . '"]);' . single_line;
       // $string .= double_tab . '$model->setProperty($' . $model["mapping"]["referenceKey"] . ');' . single_line;
        $string .= double_tab . '$model->' . $model["mapping"]["targetKey"] . '=$' . $model["mapping"]["referenceKey"] . ';' . single_line;
        $string .= double_tab . 'if(!$model->save()){' . single_line;
        $string .= double_tab . 'throw new \Exception();' . single_line;
        $string .= double_tab . '}' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    public function showRelation()
    {

    }

    public function delete()
    {
        $string = "";
        $string .= self::framePublicFunction . 'delete($id){' . single_line;
       // $string .= MethodValidationModifier::shortCutValidateDelete();
        $string .= double_tab . '$model =' . GeneralHandler::createClassNameFromTable($this->model["table"]) . 'Model::find($id);' . single_line;
        $string .= double_tab . 'if(!$model->delete()){' . single_line;
        $string .= double_tab . 'throw new \Exception();';
        $string .= double_tab . '}' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    private function setUpProperty($entity)
    {
        $property = "";
        $row = $this->con->getPropertyModel($this->model["table"]);
        foreach ($row as $item) {
            $property .= double_tab . '$model->' . $item->Field . "=" . '$input["' . $item->Field . '"];' . single_line;
        }
        return $property;
    }

    public function pagination($model)
    {
        $string = "";
        $string .= self::framePublicFunction . 'paging($input){' . single_line;
        $string .= double_tab . '$model = new ' . GeneralHandler::createClassNameFromTable($model["table"]) . 'Model();' . single_line;
        $string .= double_tab . '$model->paginating($input);' . single_line;
        $string .= double_tab . '$model=$model->getPaginate();' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

    public function show($model)
    {
        $string = "";
        $string .= self::framePublicFunction . 'show($id){' . single_line;
        $string .= double_tab . '$model = new ' . GeneralHandler::createClassNameFromTable($model["table"]) . 'Model();' . single_line;
        $string .= double_tab . '$model=$model->findById($id);' . single_line;
        $string .= double_tab . 'return $model;' . single_line;
        $string .= "\t\t}";
        return $string;
    }

}