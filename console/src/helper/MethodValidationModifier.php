<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 09/08/2017
 * Time: 10:08
 */

namespace Console\Src\Helper;


use Console\Src\Connection\Connection;
use Console\util\GeneralHandler;

class MethodValidationModifier
{
    public $model;
    protected $con;

    public function __construct($model)
    {
        $this->model = $model;
        $this->con = new Connection();
    }


    public function generateBeforeInsertValidation($model)
    {
        $string = "";
        $string .= double_tab . 'public function beforeEnterIntoDatabase(){' . single_line;
        $string .= triple_tab . $this->generatePropertyValidation();
        $string .= double_tab . 'return $this;' . single_line . "}";
        return $string;
    }

    public function generatePropertyValidation()
    {
        $string = "";
        $property = $this->con->getFieldValidation($this->model["table"]);
        foreach ($property as $item) {
            if ($item->PK_FK == "PK") {
                $string .= "";
            }
            if (($item->PK_FK == "FK") && ($item->PK_FK != "PK")) {
                $string .= $this->validationNoNull($item->COLUMN_NAME);
            } else if (($item->NULLABLE == "NO") && ($item->PK_FK != "PK")) {
                $string .= $this->validationNoNull($item->COLUMN_NAME);
            }

        }
        $string .= $this->validationRelation();
        return $string;
    }

    private function validationRelation()
    {
        $string = "";
        $relation = $this->con->getRelationModel($this->model["table"]);
        if (!$relation) {
            return "";
        } else {
            foreach ($relation as $item) {
                if ($item->PK_FK == "FK") {
                    $string .= triple_tab . '$' . GeneralHandler::createClassNameFromTable($item->REFRENCE_TABLE) . '= (new \Model\\' . GeneralHandler::createClassNameFromTable($item->REFRENCE_TABLE) . 'Model)->findById($this->' . $item->COLUMN_NAME . ');' . single_line;
                    $string .= triple_tab . 'if(empty($' . GeneralHandler::createClassNameFromTable($item->REFRENCE_TABLE) . ')){' . single_line;
                    $string .= triple_tab . 'throw new DataValidationException(DATA_VALIDATION,"' . $item->COLUMN_NAME . ' Invalid Relation ID");' . single_line;
                    $string .= single_tab . "}" . single_line;
                }
            }
        }
        return $string;
    }

    public function validationNoNull($property)
    {
        $string = "";
        $string .= 'if(empty($this->' . $property . ')){' . single_line;
        $string .= triple_tab . 'throw new DataValidationException(DATA_VALIDATION,"' . $property . ' cannot be null");' . single_line;
        $string .= double_tab . "}" . single_line;
        return $string;

    }

    public static function shortCutValidateBefore($model)
    {
        $string = double_tab . '$this->beforeInsert($input["' . GeneralHandler::createClassNameFromTable($model) . '"]);' . single_line;
        return $string;
    }

    public static function shortCutValidateDelete()
    {
        $string = double_tab . '$this->beforeDelete($id);' . single_line;
        return $string;
    }

    public static function shortCutValidateUpdate()
    {
        $string = double_tab . '$this->beforeUpdate($id,$input);' . single_line;
        return $string;
    }


}