<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 12/08/2017
 * Time: 2:55
 */

namespace Console\Src;

include __DIR__ . "/vendor.php";
use Console\Src\Helper\PropertyProfiler;
use Console\util\GeneralHandler;


class DummyGenerator
{
    public $con;
    public $property;


    public function __construct()
    {
        $this->con = new Connection\Connection();
    }

    public function getInitAllTableModels()
    {
        $tables = $this->con->getAllTable();
        foreach ($tables as $model) {
            for ($i = 0; $i <= 10; $i++) {
                $this->generatingDummy($model);
            }

        }
    }


    public function generatingDummy($table)
    {
        $tableClass = '\Model\\' . GeneralHandler::createClassNameFromTable($table->TABLE_NAME) . 'Model';
        $model = new $tableClass();
        $property = $this->con->getRelationShipDetailWithAttribute($table->TABLE_NAME);

        foreach ($property as $prop) {
            if ($prop->PK_FK != "PK") {
                $attribute = $prop->COLUMN_NAME;
                if ($prop->PK_FK == "FK") {
                    $model->$attribute = $this->dummyRelation($prop, $prop->REFERENCED_COLUMN_NAME);
                } else {
                    $model->$attribute = $this->prepareDummData($prop);
                }
            }
        }

        if (!$model->save()) {
            echo "something shit happen";
        }

    }

    public function dummyRelation($refenceTable, $referenceKey)
    {

        $table = '\Model\\' . GeneralHandler::createClassNameFromTable($refenceTable->REFERENCED_TABLE_NAME) . 'Model';
        if (empty($refenceTable->REFERENCED_TABLE_NAME)) {
            echo "macet";
        }
        $model = new $table();
        $getingData = $model->getFirst();
        $fk = $model->getKeyName();
        if ($getingData) {
            return $getingData->$fk;
        }

        $property = $this->con->getRelationShipDetailWithAttribute($refenceTable->REFERENCED_TABLE_NAME);
        foreach ($property as $prop) {
            if ($prop->PK_FK != "PK") {
                $attribute = $prop->COLUMN_NAME;
                if ($prop->PK_FK == "FK") {
                    $model->$attribute = $this->dummyRelation($prop, $prop->REFERENCED_COLUMN_NAME);
                } else {
                    $model->$attribute = $this->prepareDummData($prop);
                }
            }
        }

        if ($model->save()) {
            echo " generated 2", PHP_EOL;
            return $model->$referenceKey;
        } else {
            echo "something shit happen";
        }
    }

    private function prepareDummData($prop)
    {
        $profiler = new PropertyProfiler();
        return $profiler->filteringProperty($prop);


    }

}