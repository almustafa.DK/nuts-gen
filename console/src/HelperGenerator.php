<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 10:22
 */

namespace Console\Src;


use Console\Src\Connection\Connection;
use Console\Src\Helper\MethodModifer;
use Console\util\GeneralHandler;
use Console\util\StubGenerator;

class HelperGenerator
{
	public $con;
	public $targetDir;
	public $initializedModel;
	public $nameSpace;
	public $modelNameSpace;
	public $targetModel;
	public $baseModel;

	public $storeFunction = "";
	public $updateFunction = "";
	public $deleteFunction = "";
	public $showFunction = "";
	public $paginationFunction = "";
	public $relationUp = "";
	public $relationDown = "";

	public function __construct()
	{
		$this->con = new Connection();
		$this->initializedModel = $GLOBALS["config"]["model"];
		$this->targetDir = __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/helper/";
		$this->nameSpace = "Helper";
	}

	public function getAllInitHelper()
	{
		foreach ($this->initializedModel as $item) {
			echo "NUTS creating helper " . GeneralHandler::createClassNameFromTable($item["table"]) . "Helper", PHP_EOL;
			$this->baseModel = 'Model\\' . GeneralHandler::createClassNameFromTable($item["table"]) . 'Model';
			$this->buildSchemaClassController($item);
		}
	}

	private function buildSchemaClassController($model)
	{
		$targetClass = GeneralHandler::createClassNameFromTable($model["table"]);
		$target = $this->targetDir . "/" . $targetClass . "Helper.php";
		$this->generatePropertyFunction($model);
		$stub = new StubGenerator(
			__DIR__ . '/../stub/HelperStub.stub',
			$target
		);
		$stub->render([
			':NAMESPACE:' => $this->nameSpace,
			':CLASS_NAME:' => $targetClass."Helper",
			':MODEL_NAME:' => $targetClass . "Model",
			':PROPERTY_RELATION:' => $this->generatePropertyRelation($model),
			//':ALIAS_PROPERTY:' => '',//$this->generatingProperty(),
			':PAGINATION_FUNCTION:' => $this->paginationFunction,
			':SHOW_FUNCTION:' => $this->showFunction,
			':INSERT_FUNCTION:' => $this->storeFunction,
			':INSERT_RELATION_FUNCTION:' => $this->relationDown,
			':INSERT_FROM_RELATION_FUNCTION:' => $this->relationUp,
			':UPDATE_FUNCTION:' => $this->updateFunction,
			':DELETE_FUNCTION:' => $this->deleteFunction,
		]);
		$this->relationUp = "";
		$this->relationDown = "";
		$this->storeFunction = "";
	}

	private function generatePropertyFunction($model)
	{

		//$this->relationUpGenerator($model);
		//->relationDownGenerator($model);
		$function = new MethodModifer($model);
		$this->storeFunction = $function->store();
		$this->updateFunction = $function->update();
		$this->deleteFunction = $function->delete();
		$this->paginationFunction = $function->pagination($model);
		$this->showFunction = $function->show($model);

	}

	public function relationUpGenerator($model)
	{
		if ($this->relationUp == "") {
			if (is_array($model["relationUp"])) {
				if (count($model["relationUp"]) > 0) {
					$this->relationUp = "";
					foreach ($model["relationUp"] as $key) {
						if ($key["relationExecute"] == true) {
							$function = new MethodModifer($model);
							$this->relationUp .= $function->storeFromRelation($model, $key);
						}
					}
				}
			}
		}
	}

	public function relationDownGenerator($model)
	{

		if ($model["relationExecute"] == true) {
			$function = new MethodModifer($model);
			$this->relationDown .= $function->storeWithRelation($model);
		}
	}

//generating property class base on relation object
	private function generatePropertyRelation($model)
	{
		$property = [];
		if (!empty($model["relation"])) {
			if ((!empty($model["relation"])) && (!empty($model["relation_execution"])))
				if ($model["relation_execution"] == true)
					if (is_array($model["relation"])) {
						foreach ($model["relation"] as $key) {
							$target = '"\\Helper\\' . $key["alias"] . 'Helper"';
							$property[] = $target;
						}
					}
		}
		return implode(",", $property);

	}


}