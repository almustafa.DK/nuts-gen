<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 16:05
 */

namespace Console\Src\Connection;


use Carbon\Carbon;
use PDO;

class Connection
{
    protected $db;

    public function connection()
    {
        $driver = $GLOBALS["config"]["db"]["driver"];
        $dbhost = $GLOBALS["config"]["db"]["host"];
        $dbuser = $GLOBALS["config"]["db"]["username"];
        $dbpass = $GLOBALS["config"]["db"]["password"];
        $dbname = $GLOBALS["config"]["db"]["database"];

        $mysql_conn_string = "$driver:host=$dbhost;dbname=$dbname";
        $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db = $dbname;
        return $dbConnection;
    }


    public function getPropertyModel($model)
    {
        $db =  $GLOBALS["config"]["db"]["database"];
        $query = "select c.COLUMN_NAME,
c.IS_NULLABLE as 'NULLABLE', c.DATA_TYPE as 'DATA_TYPE',
  case
	when c.COLUMN_KEY = 'PRI' then 'PK'
	when c.COLUMN_KEY = 'MUL' then 'FK'
	else '-'
end 'PK_FK',
CASE
	when c.data_type like '%int%' then cast(c.NUMERIC_PRECISION as CHAR)
	when c.data_type like '%char%' then
	(case c.CHARACTER_MAXIMUM_LENGTH
		when -1 then 'Max'
		else cast(c.CHARACTER_MAXIMUM_LENGTH as CHAR)
	end)
when c.data_type like '%date%' then ''
else ''
end as 'DATA_LENGTH'
from INFORMATION_SCHEMA.COLUMNS c
 where c.TABLE_NAME = '$model'  and c.Table_schema = '$db'
GROUP BY c.COLUMN_NAME;";
        $conn = $this->connection()->query($query);
        try {
            $data = $conn->fetchAll(PDO::FETCH_OBJ);
            return $data;
        } catch (\Exception $exception) {
            echo Carbon::now()->toDateTimeString() . $exception->getMessage();
        }

    }

    public function getRelationModel($model)
    {
        $db =  $GLOBALS["config"]["db"]["database"];
        $query = "select c.COLUMN_NAME,cu.REFERENCED_TABLE_NAME as REFRENCE_TABLE,cu.REFERENCED_COLUMN_NAME as REFERENCED_COLUMN,
case
	when c.COLUMN_KEY = 'MUL' then CONCAT(cu.REFERENCED_TABLE_NAME, '.', cu.REFERENCED_COLUMN_NAME)
end  'RELATION',
case
	when c.COLUMN_KEY = 'PRI' then 'PK'
	when c.COLUMN_KEY = 'MUL' then 'FK'
	else '-'
end 'PK_FK'
from INFORMATION_SCHEMA.COLUMNS c
 join INFORMATION_SCHEMA.KEY_COLUMN_USAGE cu ON c.TABLE_NAME = cu.TABLE_NAME AND c.COLUMN_NAME = cu.COLUMN_NAME AND cu.TABLE_SCHEMA = c.Table_schema
where c.TABLE_NAME = '$model'  and c.Table_schema = '$db'
GROUP BY c.COLUMN_NAME;
";

        $conn = $this->connection()->query($query);
        try {
            $data = $conn->fetchAll(PDO::FETCH_OBJ);
            return $data;
        } catch (\Exception $exception) {
            echo Carbon::now()->toDateTimeString() . $exception->getMessage();
        }

    }

    public function getFieldValidation($model)
    {
        $db =  $GLOBALS["config"]["db"]["database"];
        $query = "select c.COLUMN_NAME,
c.IS_NULLABLE as 'NULLABLE', c.DATA_TYPE as 'DATA_TYPE',
  case
	when c.COLUMN_KEY = 'PRI' then 'PK'
	when c.COLUMN_KEY = 'MUL' then 'FK'
	else '-'
end 'PK_FK',

CASE
	when c.data_type like '%int%' then cast(c.NUMERIC_PRECISION as CHAR)
	when c.data_type like '%char%' then
	(case c.CHARACTER_MAXIMUM_LENGTH
		when -1 then 'Max'
		else cast(c.CHARACTER_MAXIMUM_LENGTH as CHAR)
	end)
when c.data_type like '%date%' then ''
else ''
end as 'DATA_LENGTH'
from INFORMATION_SCHEMA.COLUMNS c
 where c.TABLE_NAME = '$model'  and c.Table_schema = '$db'
GROUP BY c.COLUMN_NAME;
";
        $conn = $this->connection()->query($query);
        $data = $conn->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }


    public function getAllTable()
    {
        $model = $GLOBALS["config"]["db"]["database"];
        $query = "SELECT TABLE_NAME AS TABLE_NAME 
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_SCHEMA = '$model'";
        $conn = $this->connection()->query($query);
        $data = $conn->fetchAll(PDO::FETCH_OBJ);
        return $data;

    }

    public function getDetailPropertyTable($table)
    {
        $db =  $GLOBALS["config"]["db"]["database"];
        $query = "select c.TABLE_NAME ,c.COLUMN_NAME ,
case
	when c.COLUMN_KEY = 'PRI' then 'Primary Key'
	when c.COLUMN_KEY = 'MUL' then CONCAT('Foreign Key ke ', cu.REFERENCED_TABLE_NAME, '.', cu.REFERENCED_COLUMN_NAME)
	else CONCAT('Field untuk ', c.COLUMN_NAME)
end  'Description',
c.IS_NULLABLE as 'NULLABLE', c.DATA_TYPE as 'DATA_TYPE',
CASE
	when c.data_type like '%int%' then cast(c.NUMERIC_PRECISION as CHAR)
	when c.data_type like '%char%' then
	(case c.CHARACTER_MAXIMUM_LENGTH
		when -1 then 'Max'
		else cast(c.CHARACTER_MAXIMUM_LENGTH as CHAR)
	end)
when c.data_type like '%date%' then ''
else ''
end as 'LENGTH',
case
	when c.COLUMN_KEY = 'PRI' then 'PK'
	when c.COLUMN_KEY = 'MUL' then 'FK'
	else '-'
end 'PK_FK'
from INFORMATION_SCHEMA.COLUMNS c
left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE cu ON c.TABLE_NAME = cu.TABLE_NAME AND c.COLUMN_NAME = cu.COLUMN_NAME AND cu.TABLE_SCHEMA = c.Table_schema
where c.TABLE_NAME = '$table' and c.Table_schema = '$db'
order by c.TABLE_NAME, c.ORDINAL_POSITION;";
        $conn = $this->connection()->query($query);
        $data = $conn->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }

    public function getRelationShipDetailWithAttribute($model)
    {
        $db =  $GLOBALS["config"]["db"]["database"];
        $query = "select c.COLUMN_NAME,
	cu.REFERENCED_TABLE_NAME, cu.REFERENCED_COLUMN_NAME,
c.IS_NULLABLE as 'NULLABLE', c.DATA_TYPE,
CASE
	when c.data_type like '%int%' then cast(c.NUMERIC_PRECISION as CHAR)
	when c.data_type like '%char%' then
	(case c.CHARACTER_MAXIMUM_LENGTH
		when -1 then 'Max'
		else cast(c.CHARACTER_MAXIMUM_LENGTH as CHAR)
	end)
when c.data_type like '%date%' then ''
else ''
end as 'LENGTH',
case
	when c.COLUMN_KEY = 'PRI' then 'PK'
	when c.COLUMN_KEY = 'MUL' then 'FK'
	else '-'
end 'PK_FK'
from INFORMATION_SCHEMA.COLUMNS c
left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE cu ON c.TABLE_NAME = cu.TABLE_NAME AND c.COLUMN_NAME = cu.COLUMN_NAME AND cu.TABLE_SCHEMA = c.Table_schema
where c.TABLE_NAME = '$model' and c.Table_schema = '$db'
order by c.TABLE_NAME, c.ORDINAL_POSITION;";
        $conn = $this->connection()->query($query);
        $data = $conn->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }
}