<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 10:22
 */

namespace Console\Src;

use Console\Src\Connection\Connection;
use Console\Src\Helper\MethodValidationModifier;
use Console\Src\Helper\ModelMethodModifier;
use Console\util\GeneralHandler;
use Console\util\StubGenerator;

class ModelGenerator
{
	public $con;
	public $targetDir;
	public $initializedModel;
	public $nameSpace;
	public $targetModel;

	public function __construct()
	{
		$this->con = new Connection();
		$this->initializedModel = $GLOBALS["config"]["model"];
		$this->targetDir = __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/model/";
		$this->nameSpace = "Model";
	}

	public function getAllInitModel()
	{
		foreach ($this->initializedModel as $model) {
			echo "NUTS creating Model " . GeneralHandler::createClassNameFromTable($model["table"]) . "Model", PHP_EOL;
			$this->buildSchemaClassModel($model);
		};
	}

	public function buildSchemaClassModel($model)
	{
		$targetModel = GeneralHandler::createClassNameFromTable($model["table"]);
		$target = $this->targetDir . "/" . $targetModel . "Model.php";
		$stub = new StubGenerator(
			__DIR__ . '/../stub/ModelStub.stub',
			$target
		);
		$stub->render([
			':CLASS_NAME:' => $targetModel,
			':TABLE_NAME:' => $model["table"],
			':ALIASTABLE:' => !empty($model["aliasTable"]) ? $model["aliasTable"] : $model["table"],
			":PROPERTY:" => $this->generatingProperty($model["table"]),
			':NAMESPACE:' => $this->nameSpace,
			":RELATION:" => $this->generateRelation($model),
			':BEFORE_ENTER_INTO_TABLE:' => $this->generateValidationTable($model)
		]);
	}

	private function generatingProperty($tabel)
	{
		$string = "";
		$row = $this->con->getPropertyModel($tabel);
		if (empty($row)) {
			return "";
		}
		$i = 0;
		$totalData = count($row);
		$propertyFillable = [];
		foreach ($row as $index) {
			if ($index->PK_FK == "PK") {
				$string .= "\t" . 'protected $primaryKey="' . $index->COLUMN_NAME . '";' . "\n";
			} else {
				$propertyFillable[] = '"' . $index->COLUMN_NAME . '"';
			}
			$i++;
		}
		$berak = implode(",", $propertyFillable);
		$string .= "\t" . 'protected $fillable=[' . $berak . '];';
		return $string;
	}

	private function generateRelation($model)
	{
		$relation = "";
		$join = $this->con->getRelationModel($model["table"]);
		if ($join) {
			foreach ($join as $attribute) {
				$tableRelation = new ModelMethodModifier($model, $attribute);
				$relation .= $tableRelation->createJoinRelationShip();
			}
		}
		return $relation;
	}

	private function generateValidationTable($model)
	{
		$validationGenerator = new MethodValidationModifier($model);
		return $validationGenerator->generateBeforeInsertValidation($model);
	}

    private function generatePublicProperty($table)
    {
        $string = "";
        $row = $this->con->getPropertyModel($table);
        if (empty($row)) {
            return "";
        }
        $i = 0;
        $totalData = count($row);
        $propertyFillable = [];
        foreach ($row as $index) {
            $propertyFillable[] = '$' . $index->COLUMN_NAME . '';
            $i++;
        }
        $berak = implode(",", $propertyFillable);
        $string .= "\t" . 'public ' . $berak . ';';
        return $string;
    }
}