<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 22:38
 */

namespace Console\Src;


use Console\Src\Connection\Connection;
use Console\util\GeneralHandler;
use Console\util\StubGenerator;

class ControllerGenerator
{
	public $con;
	public $targetDir;
	public $initializedModel;
	public $nameSpace;
	public $modelNameSpace;
	public $baseHelper;
	public $targetModel;
	public $baseModel;

	public function __construct()
	{
		$this->con = new Connection();
		$this->initializedModel = $GLOBALS["config"]["model"];
		$this->targetDir = __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/controller/";
		$this->nameSpace = "Controller";

	}

	public function getAllInitController()
	{
		foreach ($this->initializedModel as $model) {
			echo "NUTS creating controller " . GeneralHandler::createClassNameFromTable($model["table"]) . "Controller", PHP_EOL;
			$this->baseModel = '\Model\\' . GeneralHandler::createClassNameFromTable($model["table"]) . 'Model';
			$this->baseHelper = '\\Helper\\' . GeneralHandler::createClassNameFromTable($model["table"]) . 'Helper';
			//$this->baseModel = $model["alias"] . 'Model';
			$this->buildSchemaClassController($model);
		};
	}

	private function buildSchemaClassController($model)
	{
		$targetController = GeneralHandler::createClassNameFromTable($model["table"]);
		$target = $this->targetDir . "/" . $targetController . "Controller.php";
		$stub = new StubGenerator(
			__DIR__ . '/../stub/ControllerStub.stub',
			$target
		);
		$stub->render([
			':NAMESPACE:' => $this->nameSpace,
			':CLASS_NAME:' => $targetController,
			':BASEMODEL:' => $this->baseModel,
			':BASEHELPER:' => $this->baseHelper
		]);
	}
}