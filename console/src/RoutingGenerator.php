<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 08/08/2017
 * Time: 14:24
 */

namespace Console\Src;


use Console\util\GeneralHandler;
use Console\util\StubGenerator;

class RoutingGenerator
{

	public $routingList = "";

	public function __construct()
	{
		$this->initializedModel = $GLOBALS["config"]["model"];
		$this->targetDir = __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/";

	}

	public function getInitAllRouting()
	{
		$this->populateRouting();
		$target = $this->targetDir . "/index.php";
		$stub = new StubGenerator(
			__DIR__ . '/../stub/index.stub',
			$target
		);
		$stub->render([
			':ROUTE_API:' => $this->routingList,
		]);
	}

	private function populateRouting()
	{
		/*
		 * $app->group("/api", function () use ($app) {
		 */
		$this->routingList .= "\t" . '$app->group("/' . lcfirst(urldecode($GLOBALS["config"]["project-name"])) . '/",function()use($app){' . "\n\t";
		foreach ($this->initializedModel as $routing) {
			$this->routingList .= '$app->group("' . $routing["routingName"] . '",function()use($app){' .
				$this->groupingSubGroupRouting() .
				'$app->get("[/]","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:index");' . "\n" .
				'$app->post("[/]","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:store");' . "\n" .
				'$app->post("/paging","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:paging");' . "\n" .
				'$app->delete("/{id}/delete","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:delete");' . "\n" .
				'$app->put("/{id}/update","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:update");' . "\n" .
				'$app->get("/{id}","Controller\\' . GeneralHandler::createClassNameFromTable($routing["table"]) . 'Controller:show");' . "\n" .
				'});' . "\n";
		}
		$this->routingList .= '});';
		return $this->routingList;
	}

	private function groupingSubGroupRouting()
	{

	}
}