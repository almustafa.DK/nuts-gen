<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 15/08/2017
 * Time: 20:23
 */
include __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/vendor/autoload.php";
include __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/config/env.php";
$app = new \Slim\App($environment['development']['settings']);
$container = $app->getContainer();
$container["app"] = $environment['development'];
require __DIR__ . "/../../app/" . $GLOBALS["config"]["project-name"] . "/config/database.php";
