<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 10/08/2017
 * Time: 20:29
 */

namespace Provider\IAM\Core\Model;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;

class CoreModel extends Model
{
	use Pagination;

	public $timestamps = false;
	public $query;
	protected $aliasTable = "";
	protected $fillable = [];

	protected $aliasColoumn = [];

	public function __construct($attributes = [])
	{
		parent::__construct($attributes);
		$this->getManager();

	}

	public function setProperty($attributes)
	{
		$this->fill($attributes);
		return $this;
	}


	public function getManager()
	{
		$alias = !empty($this->getAliasTable()) ? " as " . $this->getAliasTable() : " as " . $this->getTable();
		$this->query = Manager::table($this->getTable() . $alias);
		return $this;
	}

	public function getAliasTable()
	{
		return $this->aliasTable;
	}

	public function findData($coloumn, $operator, $value)
	{
		return $this->getManager()->whereData($coloumn, $operator, $value)->getFirst();
	}

	public function whereInData($column, $values, $boolean = 'and', $not = false)
	{
		$this->query->whereIn($column, $values, $boolean, $not);
		return $this;
	}

	public function whereDataRaw($sql, array $bindings = [], $boolean = 'and')
	{
		$this->query->whereRaw($sql, $bindings, $boolean);
		return $this;
	}

	public function whereData($column, $operator = null, $value = null, $boolean = 'and')
	{
		$this->query->where($column, $operator, $value, $boolean);
		return $this;
	}

	public function toSql()
	{
		return $this->query->toSql();
	}

	public function getFirst()
	{
		return $this->query->first();

	}

	public function findById($id, $columns = ['*'])
	{
		$this->query->where($this->aliasTable . "." . $this->primaryKey, "=", $id);
		return $this->query->first();
	}

	public function selectData($columns)
	{
		$this->query->select($columns);
		return $this;
	}

	public function countData()
	{
		return $this->query->count();

	}

	public function selectRawData($expression, array $bindings = [])
	{
		$this->query->selectRaw($expression, $bindings);
		return $this;
	}

	public function withRelation()
	{
		$this->callRelation();
		return $this;
	}

	public function getData()
	{

		return $this->query->get();
	}

	public function unionAllData($query)
	{
		$this->query->unionAll($query, true);
		return $this;
	}

	public function beforeEnterIntoDatabase()
	{
		return $this;
	}


	public function save(array $options = [])
	{
		$this->beforeEnterIntoDatabase();
		$query = $this->newQueryWithoutScopes();
		if ($this->exists) {
			$saved = $this->performUpdate($query, $options);
		} else {
			$saved = $this->performInsert($query, $options);
		}
		if ($saved) {
			$this->finishSave($options);
		}
		return $saved;
	}

	public function delete()
	{
		if (is_null($this->getKeyName())) {
			throw new \Exception('No primary key defined on model.');
		}
		if ($this->exists) {
			if ($this->fireModelEvent('deleting') === false) {
				return false;
			}
			$this->touchOwners();
			$this->performDeleteOnModel();
			$this->exists = false;
			return true;
		}
	}

	public function update(array $attributes = [], array $options = [])
	{
		if (!$this->exists) {
			return false;
		}
		return $this->fill($attributes)->save($options);
	}

	public function callRelation()
	{
		$class_methods = get_class_methods($this);
		foreach ($class_methods as $method_name) {
			$method = substr($method_name, 12);
			if (method_exists($this, "joinRelation" . $method)) {
				call_user_func(array($this, "joinRelation" . $method));
			}
		}
		return $this;
	}

}