<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 12/08/2017
 * Time: 0:00
 */

namespace Provider\IAM\Core\Model;


use Provider\General\GeneralHandler;

trait Pagination
{
	public $pagination = "";
	public $input = "";

	public function paginating($input)
	{
		GeneralHandler::validatePaging($input);
		$this->input = $input;
		$this->callRelation();
		$this->paging();
		return $this;
	}

	public function criteria()
	{
		if (empty($this->input["criteria"])) {
			if (is_array($this->input["criteria"]) && count($this->input["criteria"])) {
				foreach ($this->input["criteria"] as $crt) {
					$value = $crt["value"];
					$this->query->where($crt["criteria"], "like", "%$value%");
				}
			}
		}
		return $this;
	}

	public function orderingBy()
	{
		$this->query->orderBy($this->aliasTable . "." . $this->input["order"]["column"], $this->input["order"]["direction"]);
		return $this;
	}

	public function getPaginate()
	{

		$this->orderingBy();
		$count = $this->query->count();
		$pageCount = ceil($count / $this->input["pageSize"]); //
		return [
			"rows" => $this->query->get(),
			"pageSize" => $this->input["pageSize"],
			"page" => $this->input["pageSize"],
			'rowCount' => $count,
			'pageCount' => $pageCount,
		];
	}

	public function paging()
	{
		$this->query->take($this->input["pageSize"])
			->offset($this->input["pageSize"] * ($this->input["page"] - 1));
		return $this;
	}
}