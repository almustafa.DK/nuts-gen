<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 15:36
 */

namespace Provider\IAM\Core\Response;


use Slim\Http\Body;
use Slim\Http\Response;

class CoreResponse extends Response
{
    public function success($data, $status, $encodingOptions = 0)
    {
        $response = $this->withBody(new Body(fopen('php://temp', 'r+')));
        $response->body->write($json = json_encode([
            "timestamp" => (integer)microtime(true),
            "status" => json_decode($status),
            "data" => $data
        ], $encodingOptions));
        if ($json === false) {
            throw new \RuntimeException(json_last_error_msg(), json_last_error());
        }
        $responseWithJson = $response->withHeader('Content-Type', 'application/json;charset=utf-8');
        return $responseWithJson;
    }
}