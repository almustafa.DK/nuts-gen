<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 15:42
 */

namespace Provider\IAM\Core\Request;


use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use Slim\Http\Request;
use Slim\Interfaces\Http\HeadersInterface;

class CoreRequest extends Request
{
public function __construct($method, UriInterface $uri, HeadersInterface $headers, array $cookies, array $serverParams, StreamInterface $body, array $uploadedFiles)
{
    parent::__construct($method, $uri, $headers, $cookies, $serverParams, $body, $uploadedFiles);
}
}