CREATE TABLE system_group
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    description VARCHAR(255),
    createdAt DATETIME,
    updatedAt DATETIME
);