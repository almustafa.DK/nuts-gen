CREATE TABLE group_access
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    routeName VARCHAR(25) NOT NULL,
    routeLink TEXT NOT NULL,
    createdAt DATETIME,
    updatedAt DATETIME
);