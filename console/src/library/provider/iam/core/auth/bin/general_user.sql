CREATE TABLE system_users
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(25) NOT NULL,
    password VARCHAR(25) NOT NULL,
    createdAt DATETIME NOT NULL,
    updatedAt DATETIME
);
CREATE UNIQUE INDEX system_users_email_uindex ON system_users (email);