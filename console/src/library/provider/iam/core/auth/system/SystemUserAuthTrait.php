<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 14:45
 */

namespace Provider\IAM\Core\Auth\System;


use Provider\General\GeneralHandler;
use Provider\IAM\Core\Request\CoreRequest;
use Provider\IAM\Core\Response\CoreResponse;

trait SystemUserAuthTrait
{
	protected $helper;

	public function __construct()
	{
		$this->helper = new SystemUserAuthHelper();
	}


	public function createUser(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->createUser($input["data"]);
		return $response->success($data);
	}

	public function login(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->login($input["data"]);
		return $response->success($data);

	}

	public function updateProfile(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->updateProfile($args["id"], $input["data"]);
		return $response->success($data);
	}

	public function requestNewPassword(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->requestNewPassword($input["data"]);
		return $response->success($data);
	}
}