<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 15:02
 */

namespace Provider\IAM\Core\Auth\System;


use Provider\IAM\Core\Auth\Base\BaseHelperAuth;

class SystemUserAuthHelper extends BaseHelperAuth
{
	protected $baseModel = SystemUserAuthModel::class;

	public function __construct()
	{
		parent::__construct();
	}

	public function login($input)
	{
		// TODO: Implement login() method.
	}

	public function updateProfile($id,$input)
	{
		// TODO: Implement updateProfile() method.
	}

	public function createNewPassword($input)
	{
		// TODO: Implement createNewPassword() method.
	}

	public function createUser($input)
	{

	}
}