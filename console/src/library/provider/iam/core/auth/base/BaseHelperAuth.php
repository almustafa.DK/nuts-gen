<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 14:53
 */

namespace Provider\IAM\Core\Auth\Base;


abstract class BaseHelperAuth
{
	protected $baseModel;
	protected $model;

	public function __construct()
	{
		$this->model = new $this->baseModel();
	}


	public function beforeLogin($input)
	{
		return $this;
	}

	public function beforeUpdateProfile($input)
	{
		return $this;
	}

	public function beforeRequestNewPassword($input)
	{
		return $this;
	}

	public function beforeCreateNewPassword($input)
	{
		return $this;
	}

	abstract public function login($input);

	abstract public function updateProfile($id,$input);

	public function requestNewPassword($input)
	{

	}

	abstract public function createNewPassword($input);
}