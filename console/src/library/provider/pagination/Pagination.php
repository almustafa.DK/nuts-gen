<?php
/**
This project has been generated by nuts generator
nuts has been made by fahmi sulaiman
sulaimanfahmi@gmail.com
 */


namespace Provider\Pagination;


use Base\BaseHelper;
use Slim\Http\Request;

class Pagination extends BaseHelper
{
	public $pageSize;
	protected $query;
	protected $rows;
	protected $rowCount;
	protected $pageCount;

	protected $filter;
	protected $order;


	public function filterData()
	{

	}

	public function orderData()
	{

	}

	public function offsite()
	{

	}

	public function queryParamLoad()
	{
	}

	public function Paginate($query, Request $request)
	{
		$this->query = $query;
		$this->filter;
		$this->orderData();
		$this->queryParamLoad();
		$this->offsite();
		$collection = [
			'rows' => $this->query->getData(),
			'rowCount' => $this->totalData,
			'pageCount' => $this->pageCount,
			"pageSize" => $this->pageSize
		];
		return $collection;
	}


}