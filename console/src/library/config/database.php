<?php

/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 28/07/2017
 * Time: 11:35
 */

$container = $app->getContainer();
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container["app"]["database"]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
return $capsule;
