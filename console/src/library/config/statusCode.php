<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 0:16
 */


/**
 * Invalid POST ERROR
 */

define("INVALID_PAGE_FORMAT", json_encode([
    "code" => 1000,
    "message" => "INVALID_PAGE_FORMAT"
]));

define("MISSING_BODY_DATA", json_encode([
    "code" => 1001,
    "message" => "MISSING BODY DATA"
]));
define("DATA_VALIDATION", json_encode([
    "code" => 1002,
    "message" => "FIELD_CANNOT_BE_NULL"
]));

define("SUCCESS", json_encode([
    "code" => 2000,
    "message" => "SUCCESS"
]));

define("DATA_CREATED", json_encode([
    "code" => 2002,
    "message" => "DATA_CREATED"
]));

define("DATA_DELETED", json_encode([
    "code" => 2003,
    "message" => "DATA_DELETED"
]));

define("DATA_UPDATED", json_encode([
    "code" => 2004,
    "message" => "DATA_UPDATED"
]));


define("PAGE_NOT_FOUND", json_encode([
    "code" => 4000,
    "message" => "PAGE_NOT_FOUND"
]));

define("INTERNAL_SERVER_ERROR", json_encode([
    "code" => 4001,
    "message" => "INTERNAL_SERVER_ERROR"
]));

/**
 * ERROR PERMISSIONS
 */

define("UNAUTHORIZED_ACCESS", json_encode([
    "code" => 3000,
    "message" => "UNAUTHORIZED_ACCESS"
]));
define("TOKEN_ACCESS_EXPIRED", json_encode([
    "code" => 3001,
    "message" => "TOKEN_ACCESS_EXPIRED"
]));



