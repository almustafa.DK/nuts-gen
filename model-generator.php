<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 14:57
 */
require 'vendor/autoload.php';

include __DIR__ . '/config/config.php';
include __DIR__ . '/console/util/spacing.php';

use Console\Src\ModelGenerator;

$initModel = new ModelGenerator();
$initModel->getAllInitModel();