<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 15:05
 */

namespace Controller;


use Base\BaseController;
use Provider\IAM\Core\Auth\General\GeneralUserAuthTrait;

class GeneralUserController extends BaseController
{
	use GeneralUserAuthTrait;

}