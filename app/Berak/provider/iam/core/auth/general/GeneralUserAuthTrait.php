<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 14:43
 */

namespace Provider\IAM\Core\Auth\General;

use Provider\General\GeneralHandler;
use Provider\IAM\Core\Request\CoreRequest;
use Provider\IAM\Core\Response\CoreResponse;

trait GeneralUserAuthTrait
{
	public $helper;

	public function __construct()
	{
		$this->helper = new GeneralUserAuthHelper();
	}

	public function loging(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->login($input["data"]);
		return $response->success($data);
	}

	public function register(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->register($input["data"]);
		return $response->success($data);
	}

	public function updateProfile(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->updateProfile($args["id"], $input["data"]);
		return $response->success($data);
	}


	public function requestNewPassword(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->requestNewPassword($input["data"]);
		return $response->success($data);

	}

	public function createNewPassword(CoreRequest $request, CoreResponse $response, $args)
	{
		$input = $request->getParsedBody();
		GeneralHandler::validateInputData($input);
		$data = $this->helper->requestNewPassword($input["data"]);
		return $response->success($data);
	}
}