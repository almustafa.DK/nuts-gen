<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 14:51
 */

namespace Provider\IAM\Core\Auth\General;


use Provider\IAM\Core\Auth\Base\BaseHelperAuth;

class GeneralUserAuthHelper extends BaseHelperAuth
{
	protected $baseModel = GeneralUserAuthModel::class;

	public function __construct()
	{
		parent::__construct();
	}

	public function register($input)
	{

	}

	public function login($input)
	{
		// TODO: Implement login() method.
	}

	public function updateProfile($id,$input)
	{
		return;
	}

	public function requestNewPassword($input)
	{
		// TODO: Implement requestNewPassword() method.
	}

	public function createNewPassword($input)
	{
		// TODO: Implement createNewPassword() method.
	}
}