<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 13/08/2017
 * Time: 14:48
 */

namespace Provider\IAM\Core\Auth\General;


use Provider\IAM\Core\Model\CoreModel;

class GeneralUserAuthModel extends CoreModel
{
	protected $table = "general_user";
	protected $aliasTable = "gu";
	protected $primaryKey = "id";


}