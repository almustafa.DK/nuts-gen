CREATE TABLE group_permission_access
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    groupId INT,
    accessId INT,
    CONSTRAINT group_permission_access_group_access_id_fk FOREIGN KEY (groupId) REFERENCES group_access (id) ON UPDATE CASCADE,
    CONSTRAINT group_permission_access_system_group_id_fk FOREIGN KEY (groupId) REFERENCES system_group (id) ON UPDATE CASCADE
);