<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 28/07/2017
 * Time: 11:55
 */
$notFoundHandler = function ($c) {
	return function ($request, $response) use ($c) {
		return $c['response']
			->withStatus(404)
			->withHeader('Content-type', 'application/json')
			->withHeader("Access-Control-Allow-Origin", "*")
			->write(json_encode([
				"meta" => [
					"timestamp" => (integer)microtime(true),
					"status" => json_decode(PAGE_NOT_FOUND),
					"data" => null
				]
			]));
	};
};

$errorHandler = function (\Slim\Container $c) {
	return function ($request, $response, Exception $exception) use ($c) {
		if ($exception instanceof \Handler\DataValidationException) {
			return $response
				->withHeader("Access-Control-Allow-Origin", "*")
				->withJson([
					"meta" => [
						"timestamp" => (integer)microtime(true),
						"status" => [
							"code" => 1002,
							"message" => "FIELD_CANNOT_BE_NULL",
							"desc" => $exception->getMessage(),
						],
						"data" => null,
					]
				], 500);
		} else if ($exception instanceof \Handler\InternalAplicationException) {
			return $response
				->withHeader("Access-Control-Allow-Origin", "*")
				->withJson([
					"meta" => [
						"timestamp" => (integer)microtime(true),
						"status" => json_decode($exception->getMessage()),
						"data" => null,
					]
				], 500);
		} else
			// No handlers found, so just throw the exception
			return $response
				->withHeader("Access-Control-Allow-Origin", "*")
				->withJson([
					"meta" => [
						"timestamp" => (integer)microtime(true),
						"status" => [
							"code" => 2001,
							"message" => $exception->getMessage(),
							"desc" => $exception->__toString(),
						],
						"data" => null,
					]
				], 500);
	};
};

