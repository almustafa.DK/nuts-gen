<?php
$environment=[
	'development'=>[
		'settings'=>[
			'displayErrorDetails'=>'1',
			'log.debug'=>'1',
			'log.enabled'=>'1',
		],

		'database'=>[
			'driver'=>'mysql',
			'host'=>'localhost',
			'database'=>'finatra',
			'username'=>'root',
			'password'=>'',
			'charset'=>'utf8',
			'collation'=>'utf8_unicode_ci',
			'prefix'=>'',
		],

		'mail'=>[
			'MAIL_HOST'=>'smtp.gmail.com',
			'MAIL_USERNAME'=>'demogimi@gmail.com',
			'MAIL_PASSWORD'=>'gimi12345',
			'MAIL_PORT'=>'465',
			'MAIL_SMTPSecure'=>'ssl',
			'MAIL_SENDER'=>'demogimi@gmail.com',
			'MAIL_SENDER_ALIAS'=>'GimiApp',
		],

	],
	'production'=>[
		'settings'=>[
			'displayErrorDetails'=>'',
			'log.debug'=>'',
			'log.enabled'=>'',
		],

		'database'=>[
			'driver'=>'mysql',
			'host'=>'localhost',
			'database'=>'finatra',
			'username'=>'root',
			'password'=>'',
			'charset'=>'utf8',
			'collation'=>'utf8_unicode_ci',
			'prefix'=>'',
		],

		'mail'=>[
			'MAIL_HOST'=>'smtp.gmail.com',
			'MAIL_USERNAME'=>'demogimi@gmail.com',
			'MAIL_PASSWORD'=>'gimi12345',
			'MAIL_PORT'=>'465',
			'MAIL_SMTPSecure'=>'ssl',
			'MAIL_SENDER'=>'demogimi@gmail.com',
			'MAIL_SENDER_ALIAS'=>'GimiApp',
		],

	],

];