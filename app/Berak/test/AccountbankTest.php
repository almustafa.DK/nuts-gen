<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 17:57
 */

namespace Test;


class AccountbankTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $uri = "http://localhost:1234/berak/account-bank";

    public function __construct($message = '')
    {
        $this->message = $message;
        $this->client = new \GuzzleHttp\Client();
        parent::__construct('Warning');
    }

    public function testIndex()
    {
        $response = $this->client->request("GET", "http://localhost:1234/berak/account-bank");

        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testPaging()
    {
        $response = $this->client->request("POST", "http://localhost:1234/berak/account-bank" . "/paging", [
                "json" => [
                    "data" => [
                        "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                        ]
                    ]
                ]
            ]
        );
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testStore()
    {
        $response = $this->client->request("POST", "http://localhost:1234/berak/account-bank", ["json" => ["data" => ["Accountbank" => ["bankId" => "110", "familyId" => "76", "accountNo" => "33884", "accountName" => "32103", "currentBalance" => "3412", "createdAt" => "54254", "updatedAt" => "88784",]]]]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShow()
    {

    }

    public function testUpdate()
    {

    }


    public function testDelete()
    {

    }
}