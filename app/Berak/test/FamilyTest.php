<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 17:57
 */

namespace Test;


class FamilyTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $uri = "http://localhost:1234/berak/families";

    public function __construct($message = '')
    {
        $this->message = $message;
        $this->client = new \GuzzleHttp\Client();
        parent::__construct('Warning');
    }

    public function testIndex()
    {
        $response = $this->client->request("GET", "http://localhost:1234/berak/families");

        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testPaging()
    {
        $response = $this->client->request("POST", "http://localhost:1234/berak/families" . "/paging", [
                "json" => [
                    "data" => [
                        "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                        ]
                    ]
                ]
            ]
        );
        $this->assertEquals(200, $response->getStatusCode());
    }


    public function testStore()
    {
        $response = $this->client->request("POST", "http://localhost:1234/berak/families", ["json" => ["data" => [["Family" => ["familyName" => "Omnis repellat.", "email" => "connelly.delfina@bechtelar.com", "currentBalance" => "82206", "owner" => "Quos corrupti.", "status" => "1", "createdAt" => "2017-08-15 22:57:10", "updatedAt" => "2017-08-15 22:57:10",]], ["Familyusers" => ["familyId" => "76", "firstName" => "Hosea", "lastName" => "Smitham", "email" => "qschiller@yahoo.com", "password" => "Qui quidem ut.", "birthOfDate" => "2017-08-15", "createdAt" => "2017-08-15 22:57:10", "updatedAt" => "98124",]]]]]);
        echo $response->getBody();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShow()
    {

    }

    public function testUpdate()
    {

    }


    public function testDelete()
    {

    }
}