<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 17:57
 */

namespace Test;


class TransactionTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $uri = "http://localhost:1234/berak/transactions";

    public function __construct($message = '')
    {
        $this->message = $message;
        $this->client = new \GuzzleHttp\Client();
        parent::__construct('Warning');
    }

 public function testIndex()
    {
        	$response = $this->client->request("GET", "http://localhost:1234/berak/transactions");

$this->assertEquals(200,$response->getStatusCode());
    }


    public function testPaging()
    {
      $response = $this->client->request("POST","http://localhost:1234/berak/transactions". "/paging",[
                "json" => [
                    "data" => [
                        "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                        ]
                    ]
                ]
            ]
        );$this->assertEquals(200,$response->getStatusCode());
    }


    public function testStore()
    {
        	$response = $this->client->request("POST", "http://localhost:1234/berak/transactions",["json"=>["data"=>[["Transaction"=>["transactionType"=>"1","paymentType"=>"1","amount"=>"27164","description"=>"Quis possimus.","referenceNumber"=>"Dolore.","postedDate"=>"2017-08-15 22:57:10","createdAt"=>"2017-08-15 22:57:10","updatedAt"=>"2017-08-15 22:57:10","familyid"=>"76",]],			["Transactiondebit"=>["familyId"=>"76","transactionId"=>"7","accountBankId"=>"65",]]]]]);
$this->assertEquals(200,$response->getStatusCode());
    }

    public function testShow()
    {
        
    }

    public function testUpdate()
    {
        
    }


    public function testDelete()
    {
       
    }
}