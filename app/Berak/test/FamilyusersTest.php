<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 17:57
 */

namespace Test;


class FamilyusersTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $uri = "http://localhost:1234/berak/family";

    public function __construct($message = '')
    {
        $this->message = $message;
        $this->client = new \GuzzleHttp\Client();
        parent::__construct('Warning');
    }

 public function testIndex()
    {
        	$response = $this->client->request("GET", "http://localhost:1234/berak/family");

$this->assertEquals(200,$response->getStatusCode());
    }


    public function testPaging()
    {
      $response = $this->client->request("POST","http://localhost:1234/berak/family". "/paging",[
                "json" => [
                    "data" => [
                        "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                        ]
                    ]
                ]
            ]
        );$this->assertEquals(200,$response->getStatusCode());
    }


    public function testStore()
    {
        	$response = $this->client->request("POST", "http://localhost:1234/berak/family",["json"=>["data"=>["Familyusers"=>["familyId"=>"76","firstName"=>"Lloyd","lastName"=>"Hessel","email"=>"erdman.arielle@cassin.biz","password"=>"Sit esse.","birthOfDate"=>"2017-08-15","createdAt"=>"2017-08-15 22:57:10","updatedAt"=>"38496",]]]]);
$this->assertEquals(200,$response->getStatusCode());
    }

    public function testShow()
    {
        
    }

    public function testUpdate()
    {
        
    }


    public function testDelete()
    {
       
    }
}