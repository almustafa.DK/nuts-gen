<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 17:57
 */

namespace Test;


class CategoryTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $uri = "http://localhost:1234/berak/category";

    public function __construct($message = '')
    {
        $this->message = $message;
        $this->client = new \GuzzleHttp\Client();
        parent::__construct('Warning');
    }

 public function testIndex()
    {
        	$response = $this->client->request("GET", "http://localhost:1234/berak/category");

$this->assertEquals(200,$response->getStatusCode());
    }


    public function testPaging()
    {
      $response = $this->client->request("POST","http://localhost:1234/berak/category". "/paging",[
                "json" => [
                    "data" => [
                        "pageSize" => 1,
                        "page" => 1,
                        "order" => [
                            "column" => "id",
                            "direction" => "asc"
                        ]
                    ]
                ]
            ]
        );$this->assertEquals(200,$response->getStatusCode());
    }


    public function testStore()
    {
        	$response = $this->client->request("POST", "http://localhost:1234/berak/category",["json"=>["data"=>["Category"=>["Name"=>"Numquam et.","Name"=>"Veritatis.","Code"=>"Et maiores nam.","Code"=>"Dicta sit in.","DisplayImagePath"=>"Qui iusto.","DisplayImagePath"=>"Maxime quod.","BannerImagePath"=>"Voluptatum ut.","BannerImagePath"=>"Repellat quia.","Description"=>"","Description"=>"","IsActive"=>"1","IsActive"=>"1","Status"=>"1","Status"=>"1","CreatedDate"=>"2017-08-15 22:57:10","CreatedDate"=>"2017-08-15 22:57:10","UpdatedDate"=>"2017-08-15 22:57:10","UpdatedDate"=>"2017-08-15 22:57:10","CreatedBy"=>"41673","CreatedBy"=>"98445","UpdatedBy"=>"70472","UpdatedBy"=>"96837","MetaKeyword"=>"Nihil veniam.","MetaKeyword"=>"Non minus.","MetaDescription"=>"Dicta eum.","MetaDescription"=>"Labore quia.","MetaTitle"=>"Aut quia.","MetaTitle"=>"Eos aut.","FriendlyURL"=>"Aperiam vero.","FriendlyURL"=>"Eos.","ShowInMenu"=>"1","ShowInMenu"=>"1","IsShoe"=>"1",]]]]);
$this->assertEquals(200,$response->getStatusCode());
    }

    public function testShow()
    {
        
    }

    public function testUpdate()
    {
        
    }


    public function testDelete()
    {
       
    }
}