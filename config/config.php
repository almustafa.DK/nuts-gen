<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 07/08/2017
 * Time: 21:36
 */
$config = [
    "project-name" => "Berak",
    "multipleProject"=>[
        ""
    ],
    "auth" => [
        "sysLogin" => [
            "scaffolding" => true,
            "reference" => [
                "table" => "",
                "credential" => [
                    "email", "password"
                ]
            ]
        ],
        "publicLogin" => [
            "scaffolding" => true,
            "reference" => [
                "table" => "user",
                "credential" => [
                    "email", "password"
                ]
            ]
        ]
    ],
    "db" => [
        "driver" => "mysql",
        "host" => "localhost",
        "database" => "finatra",
        "username" => "root",
        "password" => "",
        "charset" => "utf8",
        "collation" => "utf8_unicode_ci",
        "prefix" => ""
    ],
    "model" => [
        [
            "table" => "family",
            "aliasTable" => "fmly",
            "routingName" => "families",
            "relationUp" => [],
            "relationDown" => [
                [
                    "table" => "familyusers",
                    "routingName" => "family",
                    "relationType" => "oneToMany",
                    "mapping" => [
                        "referenceKey" => "id",
                        "targetKey" => "familyId"
                    ],
                    "relationExecute" => true,
                    "relationDown" => []
                ],
            ],
            "relationExecute" => true,
        ],
        [
            "table" => "familyusers",
            "aliasTable" => "fmu",
            "routingName" => "family",
            "relationUp" => [
                [
                    "table" => "family",
                    "mapping" => [
                        "referenceKey" => "ID",
                        "targetKey" => "familyId"
                    ],
                    "relationExecute" => true,
                    "relationType" => "oneToOne"
                ]
            ],
            "relationDown" => [],
            "relationExecute" => false,
        ],
        [
            "table" => "accountbank",
            "routingName" => "account-bank",
            "aliasTable" => "acbnk",
            "relationUp" => [
                [
                    "table" => "family",
                    "mapping" => [
                        "referenceKey" => "ID",
                        "targetKey" => "familyId"
                    ],
                    "relationExecute" => false,
                    "relationType" => "oneToMany"
                ]
            ],
            "relationDown" => [],
            "relationExecute" => false,
        ],
        [
            "table" => "transaction",
            "aliasTable" => "trans",
            "routingName" => "transactions",
            "relationUp" => [
                [
                    "table" => "family",
                    "mapping" => [
                        "referenceKey" => "id",
                        "targetKey" => "familyId"
                    ],
                    "relationExecute" => false,
                    "relationType" => "oneToOne"
                ]
            ],
            "relationDown" => [
                [
                    "table" => "transactiondebit",
                    "routingName" => "transaction-debit",
                    "relationType" => "oneToMany",
                    "mapping" => [
                        "referenceKey" => "id",
                        "targetKey" => "familyId"
                    ],
                    "relationDown" => [],
                    "relationExecute" => true,
                ]
            ],
            "relationExecute" => true,
        ],
        [
            "table" => "banks",
            "routingName" => "banks",
            "aliasTable" => "banks",
            "relationUp" => [],
            "relationDown" => [],
            "relationExecute" => false,
        ],
        [
            "table" => "transactiondebit",
            "routingName" => "transaction-debit",
            "aliasTable" => "transd",
            "relationUp" => [
                [
                    "table" => "transaction",
                    "mapping" => [
                        "referenceKey" => "id",
                        "targetKey" => "transactionId"
                    ],
                    "relationExecute" => true,
                    "relationType" => "oneToOne"
                ]
            ],
            "relationDown" => [],
            "relationExecute" => false,
        ]

    ]
];