#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 18/08/2017
 * Time: 17:59
 */

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();
$application->setName("NutsGen v.0.0.1a");
$application->add(new \Console\Command\ProjectBuilderCommand());
$application->add(new \Console\Command\ControllerGeneratorCommand());
$application->add(new \Console\Command\ModelGeneratorCommand());
$application->add(new \Console\Command\ServiceGeneratorCommand());
$application->run();