<?php
/**
 * Created by PhpStorm.
 * User: Fahmi Sulaiman
 * Date: 14/08/2017
 * Time: 23:41
 */

require 'vendor/autoload.php';
include __DIR__ . '/config/configphp';
include __DIR__ . '/console/util/spacing.php';
include __DIR__ . '/app/' . $config['project-name'] . '/vendor/autoload.php';

$generator = new \Console\Src\UnitTestingGenerator();
$generator->getAllInitUnitTest();
$directoryProject = __DIR__ . "/app/" . $config["project-name"];

foreach ($config["model"] as $model) {
    chdir($directoryProject);
    $file = \Console\util\GeneralHandler::createClassNameFromTable($model["table"]) . "Test.php";
    exec("php vendor\phpunit\phpunit\phpunit  test\\$file --log-junit test\\result\\$file.log", $output);
}


