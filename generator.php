<?php
require 'vendor/autoload.php';
use Console\ProjectBuilder;

include __DIR__ . '/config/config.php';
include __DIR__ . '/console/util/spacing.php';


echo "nuts-generator has been started", PHP_EOL;
$builder = new ProjectBuilder();
$builder->generateProject();
die();
